﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SneakyImageUpdate.PushClass
{
    public enum NotificationServiceDistributionType
    {
        /// <summary>
        /// Loops through all connections in sequential order to ensure completely even distribution
        /// </summary>
        Sequential,
        /// <summary>
        /// Randomly chooses a connection to use
        /// </summary>
        Random
    }
}
