﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Sneaky
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IService1
    {
        #region SneakyLogin
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Login", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyLogin SneakyLogin(SneakyLogininput objsneakyinput);
        #endregion

        #region Delete
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Delete?Userfbid={Userfbid}&Friendfbid={Friendfbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Delete Delete(string Userfbid, string Friendfbid);
        #endregion

        #region Search
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/Search?fbid={fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Search search(string fbid);
        #endregion

        #region Request
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Request?Userfbid={Userfbid}&Friendfbid={Friendfbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Request request(string Userfbid, string Friendfbid);
        #endregion

        #region Approve
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Approve?Userfbid={Userfbid}&Friendfbid={Friendfbid}&Value={value}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Approve approve(string Userfbid, string Friendfbid, string value);
        #endregion

        #region SneakyInvite
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyInvite?Fromid={Fromid}&Toid={Toid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyInvite sneakyinvite(string Fromid, string Toid);
        #endregion

        #region SneakyInfo
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyInfo?FBid={Fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyInfo sneakyinfo(string FBid);
        #endregion

        #region SneakyReqDetails
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyDetails?UserID={UserID}&FriendID={FriendID}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyReqDetails sneakyreqdetails(string UserID, string FriendID);
        #endregion

        #region SneakyMembers
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyMembers?ToID={ToID}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyMembers  sneakymembers(string ToID);
        #endregion

        #region SneakyLocatioin
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyLocation?FBID={FBID}&Lat={Lat}&Lon={Lon}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyLocation sneakylocation(string FBID,string Lat,string Lon);
        #endregion

        #region ImageUpload
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Uploadimage", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ImageUpload ImageUpload(ImageUploadInput objImageUploadinput);
        #endregion



        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.


    #region Search
    [DataContract]
    public class Search
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public List<FriendDetails> frienddetails { get; set; }
        [DataMember(Order = 4)]
        public List<InvitedFriends> invitedfriends { get; set; }
    }
    [DataContract]
    public class FriendDetails
    {
        [DataMember(Order = 1)]
        public string FriendsFBID { get; set; }
        [DataMember(Order = 2)]
        public string FriendsName { get; set; }
        [DataMember(Order = 3)]
        public string FriendsPropic { get; set; }
        [DataMember(Order = 4)]
        public string Status { get; set; }
    }
    [DataContract]
    public class InvitedFriends
    {
        [DataMember(Order = 1)]
        public string FriendsFBID { get; set; }
        [DataMember(Order = 2)]
        public string FriendsName { get; set; }
        [DataMember(Order = 3)]
        public string FriendsPropic { get; set; }
        [DataMember(Order = 4)]
        public string Status { get; set; }
    }
    [DataContract]
    public class SearchInput
    {

        [DataMember]
        public string fbid { get; set; }

    }
    #endregion

    #region  Angels login
    [DataContract]
    public class SneakyLogin
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }
    [DataContract]
    public class SneakyLogininput
    {
        [DataMember]
        public string Did { get; set; }
        [DataMember]
        public string Dtoken { get; set; }
        [DataMember]
        public string fbid { get; set; }
        [DataMember]
        public string FBtoken { get; set; }
        [DataMember]
        public string Lat { get; set; }
        [DataMember]
        public string Lon { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }

    }

    [DataContract]
    public class Friendslogininput
    {
        [DataMember]
        public string Did { get; set; }
        [DataMember]
        public string Dtoken { get; set; }
        [DataMember]
        public string fbid { get; set; }
        [DataMember]
        public string FBtoken { get; set; }
        [DataMember]
        public string Lat { get; set; }
        [DataMember]
        public string Lon { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }

    }
    #endregion

    #region deletesign
    [DataContract]
    public class Delete
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region Request
    [DataContract]
    public class Request
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region Approve
    [DataContract]
    public class Approve
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region SneakyInvite
    [DataContract]
    public class SneakyInvite
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string RequestedDate { get; set; }
    }

    #endregion

    #region SneakyReqDetails
    [DataContract]
    public class SneakyReqDetails
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string ImageStatus { get; set; }
        [DataMember(Order = 4)]
        public string Imageurl { get; set; }
        [DataMember(Order = 5)]
        public string RequestedDate { get; set; }
        [DataMember(Order = 6)]
        public string AcceptedDate { get; set; }
        [DataMember(Order = 7)]
        public string Latitude { get; set; }
        [DataMember(Order = 8)]
        public string Longitude { get; set; }
        [DataMember(Order = 9)]
        public string Sneakiedtimes { get; set; }
    }
    

    #endregion

    #region Sneakyinfo
    [DataContract]
    public class SneakyInfo
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region SneakyMembers
    [DataContract]
    public class SneakyMembers
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public List<MemberDetails> memberdetails { get; set; }
    }
    [DataContract]
    public class MemberDetails
    {
        [DataMember(Order = 1)]
        public string FBID { get; set; }
        [DataMember(Order = 2)]
        public string imageurl { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string AcceptedDate { get; set; }
    }
    #endregion

    #region SneakyLocation
    [DataContract]
    public class SneakyLocation
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region  ImageUpload
    [DataContract]
    public class ImageUpload
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string SneakyimgURL { get; set; }
    }
    [DataContract]
    public class ImageUploadInput
    {
        [DataMember]
        public string FBid { get; set; }
        [DataMember]
        public string imagedata { get; set; }
    }
    #endregion

}
