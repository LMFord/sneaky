﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Configuration;
using Facebook;
using System.Drawing;

namespace Sneaky
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        #region Database Connection Declarations
        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ToString());
        SqlCommand cmd1 = null;
        
        //SqlDataAdapter sda = null;
        SqlDataReader sdr = null;
        SqlDataReader sdr2 = null;
        string query = null;
        #endregion

        #region variable declarations

        #region sneaky Login
        //Login
        SneakyLogin objSneakylogin = new SneakyLogin();
        Friendslogininput objfriendslogin = new Friendslogininput();
        public string UserID = null;
        public string FriendsFBID = null;
        string uid = null;
        string uid2 = null;
        string Fname = null;
        string Lname = null;
        string Pic = null;
        string Pic2 = null;
        string Sex = null;
        string Email = null;
        #endregion

        #region delete declaration
        Delete objdelete = new Delete();
        #endregion

        #region Request
        Request objrequest = new Request();
        #endregion

        #region Approve
        Approve objapprove = new Approve();
        #endregion

        #region Search
        Search objsearch = new Search();
        FriendDetails objFriendDetails = new FriendDetails();
        InvitedFriends objinvitedfriends = new InvitedFriends();


        #endregion

        #region SneakyInvite
        SneakyInvite objsneakyinvite = new SneakyInvite();
        #endregion

        #region SneakyReqDetails
        SneakyReqDetails objsneakyreq = new SneakyReqDetails();
        
        #endregion

        #region Sneakyinfo
        SneakyInfo objsneakyinfo = new SneakyInfo();
        #endregion

        #region SneakyLocation
        SneakyLocation objsneakylocaion = new SneakyLocation();
        #endregion

        #region SneakyMessage
        SneakyMessage objmessage = new SneakyMessage();
        #endregion

        #region SneakyMembers
        SneakyMembers objsneakymembers = new SneakyMembers();
        MemberDetails objmemberdetails = new MemberDetails();
        #endregion

        #region ImageUpload
        ImageUpload objImageUpload = new ImageUpload();
        Random random = new Random();

        #endregion

        #region CandidDetails
        CandidDetails objcandiddetails = new CandidDetails();
        Candidinfo objcandidinfo = new Candidinfo();
        #endregion

        #region CandidStatus
        CandidStatus objcandidstatus= new CandidStatus();
        #endregion
        
        #region Sneakyimagelist
        SneakyImageList objsneakyimg = new SneakyImageList();
        ImageDetails objimgdetails = new ImageDetails(); 
        #endregion

        #region Profileinfo
        Profileinfo objprofileinfo = new Profileinfo();
        #endregion

        #region MessageDetails
        MessageDetails objmsgdetails = new MessageDetails();
        ReceivedDetails objrecdetails = new ReceivedDetails();
        SentDetails objsentdetails = new SentDetails();

#endregion

        #endregion

        #region SneakyLogin
        public SneakyLogin SneakyLogin(SneakyLogininput objsneakyinput)
        {
            //writeLog("strt", "start");
            try
            {

                if (objsneakyinput.fbid != "" && objsneakyinput.FBtoken != "" && objsneakyinput.Lat != "" && objsneakyinput.Lon != "")
                {
                    SqlConnection.ClearAllPools();
                    writeLog("", objsneakyinput.fbid + objsneakyinput.FBtoken);
                    string picurl = "https://graph.facebook.com/" + objsneakyinput.fbid + "/picture?width=320&height=320";
                    string picurl2 = "https://graph.facebook.com/" + objsneakyinput.fbid + "/picture?width=100&height=100";
                    writeLog("Login Service Invoked by: '" + objsneakyinput.fbid + "'", "");
                    query = "SP_SneakyLogin";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandTimeout = 1200;
                    cmd1.CommandText = query;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@Did", objsneakyinput.Did);
                    cmd1.Parameters.AddWithValue("@Dtoken", objsneakyinput.Dtoken);
                    cmd1.Parameters.AddWithValue("@FbID", objsneakyinput.fbid);
                    cmd1.Parameters.AddWithValue("@prof_pic", picurl);
                    cmd1.Parameters.AddWithValue("@propic_med", picurl2);
                    cmd1.Parameters.AddWithValue("@FBtoken", objsneakyinput.FBtoken);
                    cmd1.ExecuteNonQuery();
                    con1.Close();
                    /// <Facebook>
                    /// Integrating the Facebook details
                    /// </Facebook>
                    var client = new FacebookClient(objsneakyinput.FBtoken);
                    #region commented
                    // writeLog("client", "fetch data");
                   // var query1 = string.Format("SELECT uid,first_name,last_name,sex,email FROM user WHERE uid = me()", objsneakyinput.fbid);
                   // dynamic parameters = new ExpandoObject();
                   // parameters.q = query1;
                   //// writeLog("client", "datacame");
                   // dynamic results = client.Get("/fql", parameters);
                   // //writeLog("client", "fqlquery");
                   // JObject friendListJson1 = JObject.Parse(results.ToString());
                   // string json1 = Convert.ToString(friendListJson1);
                   // var jss = new JavaScriptSerializer();
                   // var dict = jss.Deserialize<dynamic>(json1);
                   // var dict1 = dict["data"];
                   // foreach (var logobj in dict1)
                   // {
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["uid"])))
                   //     {
                   //         uid = "not disclosed by the user";
                   //     }
                   //     else
                   //     {
                   //         uid = Convert.ToString(logobj["uid"]);
                   //     }
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["first_name"])))
                   //     {
                   //         Fname = "not disclosed by the user";
                   //     }
                   //     else
                   //     {
                   //         Fname = Convert.ToString(logobj["first_name"]);
                   //     }
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["last_name"])))
                   //     {
                   //         Lname = "not disclosed by the user";
                   //     }
                   //     else
                   //     {
                   //         Lname = Convert.ToString(logobj["last_name"]);
                   //     }
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["uid"])))
                   //     {
                   //         Pic = "https://graph.facebook.com/" + uid + "/picture?width=320&height=320";
                   //         Pic2 = "https://graph.facebook.com/" + uid + "/picture?width=100&height=100";
                   //     }
                   //     else
                   //     {
                   //         Pic = "https://graph.facebook.com/" + uid + "/picture?width=320&height=320";
                   //         Pic2 = "https://graph.facebook.com/" + uid + "/picture?width=100&height=100";

                   //     }
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["sex"])))
                   //     {
                   //         Sex = "Not disclosed by the user";
                   //     }
                   //     else
                   //     {
                   //         Sex = Convert.ToString(logobj["sex"]);
                   //     }
                   //     if (string.IsNullOrEmpty(Convert.ToString(logobj["email"])))
                   //     {
                   //         Email = "Not disclosed by the user";
                   //     }
                   //     else
                   //     {
                   //         Email = Convert.ToString(logobj["email"]);
                   //     }
                    // }
                    #endregion
                    dynamic results = client.Get("me?fields=id,first_name,last_name,gender,email");
                    JObject friendListJson1 = JObject.Parse(results.ToString());
                    string json1 = Convert.ToString(friendListJson1);
                    var jss = new JavaScriptSerializer();
                    var dict = jss.Deserialize<dynamic>(json1);
                    string uid = null;
                    string Fname = null;
                    string Lname = dict["last_name"];
                    string Sex = dict["gender"];
                    string Pic = null;
                    string email = null;
                    if (string.IsNullOrEmpty(dict["id"]))
                    {
                        uid = "not disclosed by the user";
                    }
                    else
                    {
                        uid = Convert.ToString(dict["id"]);
                    }
                    if (string.IsNullOrEmpty(Convert.ToString(dict["first_name"])))
                    {
                        Fname = "not disclosed by the user";
                    }
                    else
                    {
                        Fname = Convert.ToString(dict["first_name"]);
                    }
                    if (string.IsNullOrEmpty(Convert.ToString(dict["last_name"])))
                    {
                        Lname = "not disclosed by the user";
                    }
                    else
                    {
                        Lname = Convert.ToString(dict["last_name"]);
                    }
                    if (string.IsNullOrEmpty(Convert.ToString(dict["id"])))
                    {
                        Pic = "http://graph.facebook.com/" + uid + "/picture?type=square";
                        Pic2 = "https://graph.facebook.com/" + uid2 + "/picture?width=100&height=100";
                    }
                    else
                    {
                        Pic = "http://graph.facebook.com/" + uid + "/picture?type=square";
                        Pic2 = "https://graph.facebook.com/" + uid2 + "/picture?width=100&height=100";
                    }
                    if (string.IsNullOrEmpty(Convert.ToString(dict["gender"])))
                    {
                        Sex = "Not disclosed by the user";
                    }
                    else
                    {
                        Sex = Convert.ToString(dict["gender"]);
                    }
                    if (string.IsNullOrEmpty(Convert.ToString(dict["email"])))
                    {
                        email = "Not disclosed by the user";
                    }
                    else
                    {
                        email = Convert.ToString(dict["email"]);
                    }
                    string Res = citystate(objsneakyinput.Lat, objsneakyinput.Lon);
                    string[] value = Res.Split(',');
                    objsneakyinput.State = value[0].ToString();
                    objsneakyinput.City = value[1].ToString();
                    if (Sex == null)
                    {
                        Sex = "Not Disclosed by the User";
                    }
                    UserID = uid;
                    query = "SP_UserDetails";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query;
                    con1.Close();
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@FbID", uid);
                    cmd1.Parameters.AddWithValue("@FName", Fname);
                    cmd1.Parameters.AddWithValue("@LName", Lname);
                    cmd1.Parameters.AddWithValue("@Picture", Pic);
                    cmd1.Parameters.AddWithValue("@pic_med", Pic2);
                    cmd1.Parameters.AddWithValue("@gender", Sex);
                 cmd1.Parameters.AddWithValue("@email", email);
                    cmd1.Parameters.AddWithValue("@Lat", objsneakyinput.Lat);
                    cmd1.Parameters.AddWithValue("@Lon", objsneakyinput.Lon);
                    cmd1.Parameters.AddWithValue("@City", objsneakyinput.City);
                    cmd1.Parameters.AddWithValue("@State", objsneakyinput.State);
                    cmd1.ExecuteNonQuery();
                    con1.Close();
                    #region commented
                    //var query2 = string.Format("SELECT uid,first_name,last_name,sex,email FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())", objsneakyinput.fbid);
                    ////writeLog("frnd", query2);
                    //dynamic parameters1 = new ExpandoObject();
                    //parameters1.q = query2;
                    //dynamic results1 = client.Get("/fql", parameters1);
                    ////writeLog("query", "fql");
                    //JObject friendListJson2 = JObject.Parse(results1.ToString());
                    //string json2 = Convert.ToString(friendListJson2);
                    //var jss1 = new JavaScriptSerializer();
                    //var dictf = jss.Deserialize<dynamic>(json2);

                    //var dictf1 = dictf["data"];
                    #endregion
                    dynamic results1 = client.Get("/me?fields=friends{first_name,id,last_name,gender}");
                    JObject friendListJson2 = JObject.Parse(results1.ToString());
                    string json2 = Convert.ToString(friendListJson2);
                    var jss1 = new JavaScriptSerializer();
                    var dictf = jss.Deserialize<dynamic>(json2);
                    var dictf1 = dictf["friends"]["data"];
                    //writeLog("dictionary", Convert.ToString(dictf1));
                    ///<Friend>
                    ///Adding Friends List from facebook
                    ///</Friend>

                    DataTable dt = new DataTable("tbl_FriendsList");
                    dt.Columns.Add("UserID", typeof(string));
                    dt.Columns.Add("FriendID", typeof(string));
                    dt.Columns.Add("FirstName", typeof(string));
                    dt.Columns.Add("LastName", typeof(string));
                    dt.Columns.Add("profile_pic", typeof(string));
                    dt.Columns.Add("profile_pic2", typeof(string));
                    dt.Columns.Add("Sex", typeof(string));
                    dt.Columns.Add("Email", typeof(string));
                    int i = 0;

                    foreach (var str in dictf1)
                    {
                        //writeLog("foreach", "fqk");
                        i++;
                        if (string.IsNullOrEmpty(Convert.ToString(str["id"])))
                        {
                            uid2 = "not disclosed by the user";
                        }
                        else
                        {
                            uid2 = Convert.ToString(str["id"]);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(str["first_name"])))
                        {
                            Fname = "not disclosed by the user";
                        }
                        else
                        {
                            Fname = Convert.ToString(str["first_name"]);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(str["last_name"])))
                        {
                            Lname = "not disclosed by the user";
                        }
                        else
                        {
                            Lname = Convert.ToString(str["last_name"]);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(str["id"])))
                        {
                            Pic = "https://graph.facebook.com/" + uid2 + "/picture?width=320&height=320";
                            Pic2 = "https://graph.facebook.com/" + uid2 + "/picture?width=100&height=100";
                        }
                        else
                        {
                            Pic = "https://graph.facebook.com/" + uid2 + "/picture?width=320&height=320";
                            Pic2 = "https://graph.facebook.com/" + uid2 + "/picture?width=100&height=100";

                        }
                        if (string.IsNullOrEmpty(Convert.ToString(str["gender"])))
                        {
                            Sex = "Not disclosed by the user";
                        }
                        else
                        {
                            Sex = Convert.ToString(str["gender"]);
                        }
                        //if (string.IsNullOrEmpty(Convert.ToString(str["email"])))
                        //{
                        //    Email = "Not disclosed by the user";
                        //}
                        //else
                        //{
                        //    Email = Convert.ToString(str["email"]);
                        //}
                        dt.Rows.Add(UserID, uid2, Fname, Lname, Pic, Pic2, Sex, Email);

                        //writeLog(UserID, uid2 + Fname);
                        con1.Open();
                             query = "SP_FrndsDetails";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query;
                        con1.Close();
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@UserID", UserID);
                        cmd1.Parameters.AddWithValue("@friendid", uid2);
                        cmd1.Parameters.AddWithValue("@Fname", Fname);
                        cmd1.Parameters.AddWithValue("@Lname", Lname);
                        cmd1.Parameters.AddWithValue("@Pic", Pic);
                        cmd1.Parameters.AddWithValue("@pic2", Pic2);
                        cmd1.Parameters.AddWithValue("@sex", Sex);
                      //  cmd1.Parameters.AddWithValue("@email", Email);
                        cmd1.ExecuteNonQuery();
                        con1.Close();
                    }
                    objSneakylogin.Message = "Login Successfull";
                    objSneakylogin.ReturnCode = "1";
                    writeLog("foreach Completed", "FriendsListCount : "+ i);
                    writeLog("Login Successfully", "");
                }
                else
                {
                    writeLog("Login Service Invoked", "Input Parameters are not Valid");
                    objSneakylogin.Message = "Login Failed: Invalid input parameters. Please Check URL";
                    objSneakylogin.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                writeLog("Service Error", ex.Message);
                objSneakylogin.ReturnCode = "2";
                objSneakylogin.Message = "Service Error: '" + ex.Message + "'";
            }
            return objSneakylogin;
        }
        #endregion

        #region Delete
        public Delete Delete(string Userfbid, string Friendfbid)
        {
            try
            {

                if (Userfbid != null & Friendfbid != null)
                {

                    writeLog("Sneaky Delete Service Invoked ", "");
                    string query2 = "SP_DELETE";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query2;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@UserFBid", Userfbid);
                    cmd1.Parameters.AddWithValue("@FriendFBid", Friendfbid);
                    cmd1.ExecuteNonQuery();
                    con1.Close();

                    objdelete.Message = "Sneaky Request Deleted Successfully";
                    objdelete.ReturnCode = "1";

                }
                else
                {
                    writeLog("Sneaky Requesst Delete Service Invoked", "Input Parameters are not Valid");
                    objdelete.Message = "Sneaky request Deleting Failed: Invalid input parameters. Please Check URL";
                    objdelete.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objdelete.ReturnCode = "2";
                objdelete.Message = "Service Error: '" + ex.Message + "'";
            }
            return objdelete;
        }
        #endregion

        #region Request
        public Request request(string Userfbid, string Friendfbid)
        {
            try
            {

                if (Userfbid != null & Friendfbid != null)
                {
                    string query = "select count(*) from tbl_FriendsList where UserID='" + Userfbid + "' and FriendID='" + Friendfbid + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        writeLog("Request Service Invoked ", "");
                        string query2 = "SP_Request";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@UserFBid", Userfbid);
                        cmd1.Parameters.AddWithValue("@FriendFBid", Friendfbid);
                        cmd1.ExecuteNonQuery();
                        con1.Close();
                        objrequest.Message = "Sneaky Request Sent Successfully";
                        objrequest.ReturnCode = "1";
                    }
                    else
                    {
                        writeLog("Sneaky Requesst  Service Invoked", "Input Parameters are not Valid");
                        objrequest.Message = "Sneaky request Sending Failed: Invalid input parameters. Please Check URL";
                        objrequest.ReturnCode = "0";
                    }

                }
                else
                {
                    writeLog("Sneaky Requesst  Service Invoked", "Input Parameters are empty");
                    objrequest.Message = "Sneaky request Sending Failed: Empty input parameters. Please Check URL";
                    objrequest.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objrequest.ReturnCode = "2";
                objrequest.Message = "Service Error: '" + ex.Message + "'";
            }
            return objrequest;
        }
        #endregion

        #region Approve
        public Approve approve(string Userfbid, string Friendfbid, string value)
        {
            try
            {

                if (Userfbid != null & Friendfbid != null & value != null)
                {
                    string query = "select count(*) from tbl_FriendsList where UserID='" + Userfbid + "' and FriendID='" + Friendfbid + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        writeLog("Approve Service Invoked ", "");
                        string query2 = "SP_Approve";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@UserFBid", Userfbid);
                        cmd1.Parameters.AddWithValue("@FriendFBid", Friendfbid);
                        cmd1.Parameters.AddWithValue("@Value", value);
                        cmd1.ExecuteNonQuery();
                        con1.Close();
                        if (value == "1")
                        {
                            objapprove.Message = "Sneaky Request approved Successfully";
                            objapprove.ReturnCode = "1";
                        }
                        else if (value == "0")
                        {
                            objapprove.Message = "Sneaky Request denied ";
                            objapprove.ReturnCode = "1";
                        }

                    }
                    else
                    {
                        writeLog("Sneaky approve  Service Invoked", "Input Parameters are not Valid");
                        objapprove.Message = "Sneaky approve Failed: Invalid input parameters. Please Check URL";
                        objapprove.ReturnCode = "0";
                    }

                }
                else
                {
                    writeLog("Sneaky approve  Service Invoked", "Input Parameters are empty");
                    objapprove.Message = "Sneaky approve Failed: Empty input parameters. Please Check URL";
                    objapprove.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objapprove.ReturnCode = "2";
                objapprove.Message = "Service Error: '" + ex.Message + "'";
            }
            return objapprove;
        }
        #endregion

        #region Search
        public Search search(string fbid)
        {

            if (fbid != null)
            {
                try
                {

                    writeLog("", "Search Invoked");
                    string query4 = "SP_Search";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query4;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@FBID", fbid);
                    sdr = cmd1.ExecuteReader();
                    if (sdr.FieldCount != 0)
                    {
                        objsearch.frienddetails = new List<FriendDetails>();
                        while (sdr.Read())
                        {
                            
                            objFriendDetails = new FriendDetails();
                            objFriendDetails.FriendsFBID = sdr[0].ToString();
                            objFriendDetails.FriendsName = sdr[1].ToString();
                            objFriendDetails.FriendsPropic = sdr[2].ToString();
                            objFriendDetails.Status = sdr[3].ToString();
                            objsearch.frienddetails.Add(objFriendDetails);
                        }

                        if (sdr.NextResult())
                        {
                            objsearch.invitedfriends = new List<InvitedFriends>();
                            while (sdr.Read())
                            {
                                objinvitedfriends = new InvitedFriends();                            
                                objinvitedfriends.FriendsFBID = sdr[0].ToString();
                                objinvitedfriends.FriendsName = sdr[1].ToString();
                                objinvitedfriends.FriendsPropic = sdr[2].ToString();
                                objinvitedfriends.Status = sdr[3].ToString();
                                objsearch.invitedfriends.Add(objinvitedfriends);
                            }
                        
                        }
                        objsearch.ReturnCode = "1";
                        objsearch.Message = "Successful response";

                    }
                   
                    else
                    {
                        objsearch.ReturnCode = "0";
                        objsearch.Message = "no friends found using the app";
                        writeLog("search share Service Invoked", "No data found");
                    }
                    
                }

                catch (Exception ex)
                {
                    con1.Dispose();
                    writeLog("Service Error", ex.Message);
                    objsearch.ReturnCode = "2";
                    objsearch.Message = "Service Error" + ex.Message;
                }
                con1.Close();

            }
            return objsearch;
        }



        #endregion

        #region SneakyInvite
        public SneakyInvite sneakyinvite(string Fromid, string Toid)
        {
            try
            {

                if (Fromid != null & Toid != null)
                {
                    string query = "select count(*) from tbl_ReqTracker where FromID='" + Fromid + "' and ToID='" + Toid + "' and Status='approved'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        writeLog("Sneaky Invite Service Invoked ", "");
                        string query2 = "SP_SneakyInvite";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;                       
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@FromID", Fromid);
                        cmd1.Parameters.AddWithValue("@ToID", Toid);
                        
                        sdr = cmd1.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                objsneakyinvite.Message = sdr[0].ToString();
                                objsneakyinvite.ReturnCode = sdr[1].ToString();
                                objsneakyinvite.RequestedDate = sdr[2].ToString();
                            }
                        } 
                        con1.Close();
                    }
                    else
                    {
                        writeLog("Sneaky Invitation  Service Invoked", "Input Parameters are not Valid");
                        objsneakyinvite.Message = "Sneaky Invitation Failed: Invalid input parameters. Please Check URL";
                        objsneakyinvite.ReturnCode = "0";
                    }

                }
                else
                {
                    writeLog("Sneaky Invitation  Service Invoked", "Input Parameters are empty");
                    objsneakyinvite.Message = "Sneaky invitation Failed: Empty input parameters. Please Check URL";
                    objsneakyinvite.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakyinvite.ReturnCode = "2";
                objsneakyinvite.Message = "Service Error: '" + ex.Message + "'";
            }
            return objsneakyinvite;
        }
        #endregion

        #region SneakyReqDetails
        public SneakyReqDetails sneakyreqdetails(string UserID, string FriendID)
        {
            try
            {

                if (UserID != null & FriendID != null)
                {

                    writeLog("SneakyDetails Service Invoked ", "");
                    string query2 = "SP_SneakyReqDetails";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query2;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@UserFBID", UserID);
                    cmd1.Parameters.AddWithValue("@FriendFBID", FriendID);
                    sdr = cmd1.ExecuteReader();
                    if (sdr.HasRows)
                    {
                        objsneakyreq.Message = "Successfull Response";
                        objsneakyreq.ReturnCode = "1";
                        
                        while (sdr.Read())
                        {


                            objsneakyreq.ImageStatus = sdr[0].ToString();
                            objsneakyreq.Imageurl = sdr[1].ToString();
                            objsneakyreq.RequestedDate = sdr[2].ToString();
                            objsneakyreq.AcceptedDate = sdr[3].ToString();
                            objsneakyreq.Latitude = sdr[4].ToString();
                            objsneakyreq.Longitude = sdr[5].ToString();
                            objsneakyreq.Sneakiedtimes = sdr[6].ToString();
                        }
                    }
                    else
                    {
                        objsneakyreq.Message = "No sneaky request details found";
                        objsneakyreq.ReturnCode = "0";

                    }
                    con1.Close();
                }

                else
                {
                    writeLog("Sneaky Requesst Details Service Invoked", "Input Parameters are not Valid");
                    objsneakyreq.Message = "Sneaky request Details Failed: Invalid input parameters. Please Check URL";
                    objsneakyreq.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakyreq.ReturnCode = "2";
                objsneakyreq.Message = "Service Error: '" + ex.Message + "'";
            }

            return objsneakyreq;
        }
        #endregion

        #region SneakyInfo
        public SneakyInfo sneakyinfo(string FBid)
        {
            try
            {

                if (FBid != null)
                {
                    string query = "select count(*) from tbl_SneakyList where ToID='" +  FBid + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        string query1 = "select count(*) from tbl_SneakyList where ImageStatus='NO' AND ToID='" + FBid + "'";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.Text;
                        cmd1.CommandText = query1;
                        con1.Open();
                        int count1 = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                        con1.Close();
                        if (count1 > 0)
                         {
                        string query2 = "SP_Sneakyinfo";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@FBID", FBid);   
                          sdr = cmd1.ExecuteReader();
                          if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                objsneakyinfo.ReturnCode = "1";
                                objsneakyinfo.Message = "You have sneaky request";
                                objsneakyinfo.por_pic = sdr[0].ToString();
                                objsneakyinfo.FirstName = sdr[1].ToString();
                                objsneakyinfo.Sneakiedtimes = sdr[2].ToString();
                                objsneakyinfo.candid_status = sdr[3].ToString();
                                if (objsneakyinfo.candid_status == "")
                                {
                                    objsneakyinfo.candid_status = "No";
                                }
                            }
                        }
                          else
                          {

                              objsneakyinfo.ReturnCode = "0";
                              objsneakyinfo.Message = "You have no sneaky request";
                          }
                        }
                        else
                        {

                            objsneakyinfo.ReturnCode = "0";
                            objsneakyinfo.Message = "You have no sneaky request";
                        }

                      
                    }

                    else
                    {

                        objsneakyinfo.ReturnCode = "0";
                        objsneakyinfo.Message = "Please enter a valid FBID";
                    }

                }
               
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakyinfo.ReturnCode = "2";
                objsneakyinfo.Message = "Service Error: '" + ex.Message + "'";
            }
            return objsneakyinfo;
        }
        #endregion

        #region SneakyMembers
        public SneakyMembers  sneakymembers(string ToID)
        {
            try
            {

                if (ToID != null)
                {

                    writeLog("Sneaky members Service Invoked ", "");
                    string query2 = "SP_SneakyMembers";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query2;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@ToID", ToID);
                    
                    sdr = cmd1.ExecuteReader();
                    if (sdr.HasRows)
                    {
                        objsneakymembers.Message = "Successfull Response";
                        objsneakymembers.ReturnCode = "1";
                        objsneakymembers.memberdetails = new List<MemberDetails>();
                        while (sdr.Read())
                        {
                            objmemberdetails = new MemberDetails();
                            objmemberdetails.FBID = sdr[0].ToString();
                            objmemberdetails.imageurl = sdr[1].ToString();
                            objmemberdetails.Name = sdr[2].ToString();
                            objmemberdetails.AcceptedDate = sdr[3].ToString();
                            objsneakymembers.memberdetails.Add(objmemberdetails);


                        }
                    }
                    else
                    {
                        objsneakymembers.Message = "No sneaky member details found";
                        objsneakymembers.ReturnCode = "0";

                    }
                    con1.Close();
                }

                else
                {
                    writeLog("Sneaky Requesst Details Service Invoked", "Input Parameters are not Valid");
                    objsneakymembers.Message = "Sneaky Members Details Failed: Invalid input parameters. Please Check URL";
                    objsneakymembers.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakymembers.ReturnCode = "2";
                objsneakymembers.Message = "Service Error: '" + ex.Message + "'";
            }

            return objsneakymembers;
        }

       #endregion

        #region SneakyLocation
        public SneakyLocation sneakylocation(string FBID, string Lat, string Lon)
        {
            try
            {

                if (FBID != null & Lat != null & Lon!=null)
                {
                    string query = "select count(*) from tbl_SneakyList where FromID='" + FBID + "' or ToID='" + FBID + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        writeLog("Sign Service Invoked ", "");
                        string query2 = "SP_SneakyLocation";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@FBID", FBID);
                        cmd1.Parameters.AddWithValue("@Lat", Lat);
                        cmd1.Parameters.AddWithValue("@Lon", Lon);
                        cmd1.ExecuteNonQuery();
                        con1.Close();
                        objsneakylocaion.Message = "Sneaky Members Location saved Successfully";
                        objsneakylocaion.ReturnCode = "1";
                    }

                    else
                    {

                        objsneakylocaion.Message = "Sneaky Members Location saving Failed: Invalid input parameters. Please Check URL";
                        objsneakylocaion.ReturnCode = "0";
                    }

                }
                else
                {
                    writeLog("Sneaky Members Location saving Service Invoked", "Input Parameters are empty");
                    objsneakylocaion.Message = "Sneaky Members Location saving Failed: Empty input parameters. Please Check URL";
                    objsneakylocaion.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakylocaion.ReturnCode = "2";
                objsneakylocaion.Message = "Service Error: '" + ex.Message + "'";
            }
            return objsneakylocaion;
        }
       
        #endregion

        #region Message
        public SneakyMessage SneakyMessage(string Fromid, string Toid, string Message)
        {
            try
            {

                if (Fromid != null & Toid != null & Message != null)
                {

                    writeLog("Sneaky Message Service Invoked ", "");
                    string query2 = "SP_Message";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query2;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@Fromid", Fromid);
                    cmd1.Parameters.AddWithValue("@Toid", Toid);
                    cmd1.Parameters.AddWithValue("@Message", Message);
                    cmd1.ExecuteNonQuery();
                    con1.Close();

                    objmessage.Message = "Message saved Successfully";
                    objmessage.ReturnCode = "1";

                }
                else
                {
                    writeLog("Sneaky Message Service Invoked", "Input Parameters are not Valid");
                    objmessage.Message = "Sneaky Message Saving Failed: Invalid input parameters. Please Check URL";
                    objmessage.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objmessage.ReturnCode = "2";
                objmessage.Message = "Service Error: '" + ex.Message + "'";
            }
            return objmessage;
        }
        #endregion
    
        #region ImageUpload
        public ImageUpload ImageUpload(ImageUploadInput objImageUploadInput)
        {
            writeLog("image", "imguploadservice");
            try
            {
                string imageurl = SaveImage(objImageUploadInput.imagedata);
                if (objImageUploadInput.FBid != "")
                {

                    writeLog("Image upload Service Invoked by: '" + objImageUploadInput.FBid + "'", "");
                    query = "SP_ImageUpload";
                    string uploadtime;
                    uploadtime = DateTime.Now.ToString();
                    /// <Random>
                    /// Generating Random Number for signid
                    /// </Random>
                    int randomNumber = random.Next();
                    /// <Statecode>
                    /// Assigning State Code
                    /// </Statecode>
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@FBid", objImageUploadInput.FBid);
                    // cmd1.Parameters.AddWithValue("@Signid", randomNumber);
                    cmd1.Parameters.AddWithValue("@ImageURL", imageurl);
                    cmd1.ExecuteNonQuery();
                    con1.Close();
                    objImageUpload.Message = "Sign Uploaded Successfully";
                    objImageUpload.ReturnCode = "1";
                    objImageUpload.SneakyimgURL = imageurl;
                }
                else
                {
                    writeLog("Sign Service Invoked", "Input Parameters are not Valid");
                    objImageUpload.Message = "Sign uploading Failed: Invalid input parameters. Please Check URL";
                    objImageUpload.ReturnCode = "0";
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objImageUpload.ReturnCode = "2";
                objImageUpload.Message = "Service Error: '" + ex.Message + "'";
            }

            return objImageUpload;
        }

        #endregion

        #region CandidDetails
        public CandidDetails CandidDetails(string Userid)
        {
            try
            {
                if (Userid != "")
                {
                    writeLog("CandidDetails invoked by ", Userid);
                    query = "SP_CandidDetails";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query;
                    cmd1.Parameters.AddWithValue("@FBid", Userid);
                    con1.Open();
                    sdr = cmd1.ExecuteReader();
                    if (sdr.HasRows)
                    {
                        objcandiddetails.Candidinfo = new List<Candidinfo>();
                        while (sdr.Read())
                        {
                            objcandiddetails.Returncode = "1";
                            objcandiddetails.Message = "Successfull Response";
                            objcandidinfo = new Candidinfo();
                            objcandidinfo.FBID = sdr[0].ToString();
                            objcandidinfo.Name = sdr[1].ToString();
                            objcandidinfo.Status = sdr[2].ToString();
                            objcandiddetails.Candidinfo.Add(objcandidinfo);

                        }
                    }
                    else
                    {
                        objcandiddetails.Returncode = "2";
                        objcandiddetails.Message = "NO Data Found or invalid FBID";
                    }


                }
                else
                {
                    objcandiddetails.Returncode = "2";
                    objcandiddetails.Message = "Enter a Valid Input";
                }
            }
            catch(Exception ex)
            {
                objcandiddetails.Returncode = "2";
                objcandiddetails.Message = ex.Message;
            }
            return objcandiddetails;
        }


        #endregion

        #region CandidStatus
        public CandidStatus CandidStatus(string Userid, string Friendid, string status)
        {
            try
            {
                if (Userid != null & Friendid != null & (status != "Yes" || status != "No"))
                {
                    writeLog("CandiddStatus by '" + Userid + "'", "For '" + Friendid + "'");
                    string query2 = "select count(*) from tbl_Candid where UserID='" + Userid + "' and FriendID='" + Friendid + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query2;
                    con1.Open();
                    int value = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (value == 0)
                    {
                        objcandidstatus.ReturnCode = "2";
                        objcandidstatus.Message = "Given FBIDs are not available";
                    }
                    else
                    {
                        string query3 = "select count(*) from tbl_Candid where UserID='" + Userid + "' and FriendID='" + Friendid + "' and CandidStatus='" + status + "'";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.Text;
                        cmd1.CommandText = query3;
                        con1.Open();
                        int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                        con1.Close();
                        if (count == 0)
                        {
                            query = "SP_CadndidStatus";
                            cmd1 = new SqlCommand();
                            cmd1.Connection = con1;
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.CommandText = query;
                            cmd1.Parameters.AddWithValue("@Userid", Userid);
                            cmd1.Parameters.AddWithValue("@Friendid", Friendid);
                            cmd1.Parameters.AddWithValue("@status", status);
                            con1.Open();
                            cmd1.ExecuteNonQuery();
                            objcandidstatus.ReturnCode = "1";
                            objcandidstatus.Message = "CandidStatus Updated Successfully";
                            con1.Close();
                        }
                        else
                        {
                            objcandidstatus.ReturnCode = "2";
                            objcandidstatus.Message = "Already You are having the same status";
                        }

                    }
                }
                else
                {
                    objcandidstatus.ReturnCode = "2";
                    objcandidstatus.Message = "Invalid input";

                }
            }
            catch (Exception ex)
            {
                objcandidstatus.ReturnCode = "2";
                objcandidstatus.Message = ex.Message;
            }
            return objcandidstatus;
        }
        #endregion

        #region SneakyImageList
        public SneakyImageList sneakyimagelist(string FBid)
        {
            try
            {

                if (FBid != null)
                {
                    string query = "select count(*) from tbl_SneakyList where FromID='" + FBid + "'";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = query;
                    con1.Open();
                    int count = Convert.ToInt32(cmd1.ExecuteScalar().ToString());
                    con1.Close();
                    if (count > 0)
                    {
                        writeLog("Sneaky Image List Service Invoked ", "");
                        string query2 = "SP_SneakyImageList";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@FBID", FBid);
                        sdr = cmd1.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            objsneakyimg.imagedetails = new List<ImageDetails>();
                            while (sdr.Read())
                            {
                                objsneakyimg.Message = "Sucessfull Response";
                                objsneakyimg.Returncode = "1";
                                objimgdetails = new ImageDetails();
                                objimgdetails.imageurl =sdr[0].ToString();
                                objimgdetails.FBID = sdr[1].ToString();
                                objimgdetails.Firstname = sdr[2].ToString();
                                objimgdetails.AcceptedDate = sdr[3].ToString();
                                objsneakyimg.imagedetails.Add(objimgdetails);
                            }
                        }
                        else
                        {
                            objsneakyimg.Message = "There is no sneaky image list for the given url";
                            objsneakyimg.Returncode = "1";
                        }
                        con1.Close();
                    }
                    else
                    {
                        objsneakyimg.Message = "Please enter a valid fbid"; ;
                        objsneakyimg.Returncode = "0";
                    }

                }
                else
                {
                    writeLog("Sneaky Invitation  Service Invoked", "Input Parameters are not Valid");
                    objsneakyimg.Message = "Sneaky Invitation Failed: Invalid input parameters. Please Check URL";
                    objsneakyimg.Returncode = "0";
                   
                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objsneakyimg.Returncode = "2";
                objsneakyimg.Message = "Service Error: '" + ex.Message + "'";
            }
            return objsneakyimg;
        }
        #endregion

        #region Profileinfo
        public Profileinfo  profileinfo(string FBid)
        {
            try
            {

                if (FBid != null)
                {
                        writeLog("Sneaky Profile Service Invoked ", "");
                        string query2 = "SP_Profileinfo";
                        cmd1 = new SqlCommand();
                        cmd1.Connection = con1;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.CommandText = query2;
                        con1.Open();
                        cmd1.Parameters.AddWithValue("@FBID", FBid);
                        sdr = cmd1.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                objprofileinfo.Message = "Sucessfull Response";
                                objprofileinfo.Returncode = "1";
                                objprofileinfo.UserName = sdr[0].ToString();
                                objprofileinfo.profilepic = sdr[1].ToString();
                                objprofileinfo.Sneaky_sent = sdr[2].ToString();
                                objprofileinfo.Sneaky_recieved = sdr[3].ToString();
                               
                            }
                        }
                        else
                        {
                            objprofileinfo.Message = "No details available for the given FBID";
                            objprofileinfo.Returncode = "1";
                        }
                        con1.Close();
                                       
                  
                }
                else
                {
                    writeLog("Sneaky Profile Service Invoked", "Input Parameters are not Valid");
                    objprofileinfo.Message = "Sneaky Profile info Failed: Invalid input parameters. Please Check URL";
                    objprofileinfo.Returncode = "0";

                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objprofileinfo.Returncode = "2";
                objprofileinfo.Message = "Service Error: '" + ex.Message + "'";
            }
            return objprofileinfo;
        }
        #endregion

        #region MessageDetails
        public MessageDetails messagedetails(string FBid)
        {
            try
            {

                if (FBid != null)
                {
                    writeLog("Sneaky Message Details Service Invoked ", "");
                    string query2 = "SP_MessageDetails";
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con1;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = query2;
                    con1.Open();
                    cmd1.Parameters.AddWithValue("@FBID", FBid);
                    sdr = cmd1.ExecuteReader();
                    if (sdr.HasRows)
                    {
                        objmsgdetails.sentdetails = new List<SentDetails>();
                        while (sdr.Read())
                        {
                            objmsgdetails.Message = "Sucessfull Response";
                            objmsgdetails.Returncode = "1";
                            objsentdetails = new SentDetails();
                            objsentdetails.FBID = sdr[0].ToString();
                            objsentdetails.Message = sdr[1].ToString();
                            objsentdetails.First_name = sdr[2].ToString();
                            objsentdetails.Prof_pic = sdr[3].ToString();
                            objmsgdetails.sentdetails.Add(objsentdetails);

                        }
                    }
                    if (sdr.NextResult())
                    {
                        objmsgdetails.recieveddetails = new List<ReceivedDetails>();
                        while (sdr.Read())
                        {
                            objmsgdetails.Message = "Sucessfull Response";
                            objmsgdetails.Returncode = "1";
                            objrecdetails = new ReceivedDetails();
                            objrecdetails.FBID = sdr[0].ToString();
                            objrecdetails.Message = sdr[1].ToString();
                            objrecdetails.First_name = sdr[2].ToString();
                            objrecdetails.Prof_pic = sdr[3].ToString();
                            objmsgdetails.recieveddetails.Add(objrecdetails);

                        }
                    }
                    else
                    {
                        objmsgdetails.Message = "No details available for the given FBID";
                        objmsgdetails.Returncode = "1";
                    }
                    con1.Close();


                }
                else
                {
                    writeLog("Sneaky Message Details Service Invoked", "Input Parameters are not Valid");
                    objmsgdetails.Message = "Sneaky Message Details Failed: Invalid input parameters. Please Check URL";
                    objmsgdetails.Returncode = "0";

                }
            }
            catch (Exception ex)
            {
                con1.Dispose();
                writeLog("Service Error", ex.Message);
                objprofileinfo.Returncode = "2";
                objprofileinfo.Message = "Service Error: '" + ex.Message + "'";
            }
            return objmsgdetails;
        }
        #endregion


        #region imageconvert
        public string SaveImage(string base64)
        {
            Random rand1 = new Random();
            int strRandNo = rand1.Next();
            string val1 = strRandNo.ToString();
            string Localpath2;
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64)))
            {
                using (Bitmap bm2 = new Bitmap(ms))
                {
                    Localpath2 = @"C:\Sneaky\Sneaky\SneakyImage\" + val1 + ".jpg";
                    writeLog("Image Name", val1);
                    bm2.Save(Localpath2);
                }
            }
            string url1 = "http://184.175.65.98/Sneaky/SneakyImage/" + val1 + ".jpg";
            return url1;
        }
        #endregion
        
        #region citystate
        public static string citystate(string latitude, string longitude)
        {
            string Address_administrative_area_level_1;
            string Address_administrative_area_level_2;
            string smallcode;
            string longcode;
            Address_administrative_area_level_1 = "";
            smallcode = "";
            longcode = "";
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("http://maps.googleapis.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false");
                XmlNode element = doc.SelectSingleNode("//GeocodeResponse/status");
                if (element.InnerText == "ZERO_RESULTS")
                {
                    return ("No data available for the specified location");
                }
                else
                {
                    element = doc.SelectSingleNode("//GeocodeResponse/result/formatted_address");
                    string longname = "";
                    string shortname = "";
                    string typename = "";
                    //bool fHit = false;
                    XmlNodeList xnList = doc.SelectNodes("//GeocodeResponse/result/address_component");
                    foreach (XmlNode xn in xnList)
                    {
                        try
                        {
                            longname = xn["long_name"].InnerText;
                            shortname = xn["short_name"].InnerText;
                            typename = xn["type"].InnerText;
                            if (typename == "administrative_area_level_1")
                            {
                                Address_administrative_area_level_1 = longname;
                                smallcode = shortname;

                            }
                            if (typename == "administrative_area_level_2")
                            {
                                Address_administrative_area_level_2 = longname;
                                longcode = shortname;
                            }
                        }
                        catch (Exception)
                        {
                            //Node missing either, longname, shortname or typename
                            //fHit = false;
                        }
                    }
                    string comb = Address_administrative_area_level_1 + "," + longcode;
                    //Console.ReadKey();
                    return comb;
                }

            }
            catch (Exception ex)
            {
                return ("(Address lookup failed: ) " + ex.Message);
            }
        }
        #endregion

        #region Service Log
        private void writeLog(string MsgID, string Msg)
        {
            string openString = DateTime.Now.ToString();
            string path = @"C:\Sneaky\log\" + DateTime.Now.ToString("dd-MM-yy") + ".txt";
            if (File.Exists(path))
            {
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(openString + " -- (" + MsgID + ") -- " + Msg);
                    writer.Close();
                }
            }
            else
            {
                File.WriteAllText(path, openString + " Service Started");
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(openString + " -- (" + MsgID + ") -- " + Msg);
                    writer.Close();
                }
            }
        }
        #endregion
    }
}

