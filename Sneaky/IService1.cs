﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Sneaky
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IService1
    {
        #region SneakyLogin
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Login", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyLogin SneakyLogin(SneakyLogininput objsneakyinput);
        #endregion

        #region Delete
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Delete?Userfbid={Userfbid}&Friendfbid={Friendfbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Delete Delete(string Userfbid, string Friendfbid);
        #endregion

        #region Search
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/Search?fbid={fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Search search(string fbid);
        #endregion

        #region Request
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Request?Userfbid={Userfbid}&Friendfbid={Friendfbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Request request(string Userfbid, string Friendfbid);
        #endregion

        #region Approve
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Approve?Userfbid={Userfbid}&Friendfbid={Friendfbid}&Value={value}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Approve approve(string Userfbid, string Friendfbid, string value);
        #endregion

        #region SneakyInvite
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyInvite?Fromid={Fromid}&Toid={Toid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyInvite sneakyinvite(string Fromid, string Toid);
        #endregion

        #region SneakyInfo
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyInfo?FBid={Fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyInfo sneakyinfo(string FBid);
        #endregion

        #region SneakyReqDetails
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyDetails?UserID={UserID}&FriendID={FriendID}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyReqDetails sneakyreqdetails(string UserID, string FriendID);
        #endregion

        #region SneakyMembers
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyMembers?ToID={ToID}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyMembers  sneakymembers(string ToID);
        #endregion

        #region SneakyLocatioin
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyLocation?FBID={FBID}&Lat={Lat}&Lon={Lon}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyLocation sneakylocation(string FBID,string Lat,string Lon);
        #endregion

        #region Message
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyMessage?Fromid={Fromid}&Toid={Toid}&Message={Message}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyMessage SneakyMessage(string Fromid, string Toid, string Message);
        #endregion

        #region ImageUpload
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Uploadimage", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ImageUpload ImageUpload(ImageUploadInput objImageUploadinput);
        #endregion

        #region Candiddetails
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/CandidDetails?Userid={Userid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        CandidDetails CandidDetails(string Userid);
        #endregion

        #region candidstatus
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Candidstatus?Userid={Userid}&Friendid={Friendid}&status={status}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        CandidStatus CandidStatus(string Userid, string Friendid, string status);
        #endregion

        #region SneakyImageList
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SneakyImageList?FBid={Fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SneakyImageList sneakyimagelist(string FBid);
        #endregion


        #region Profileinfo
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/Profileinfo?FBid={Fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Profileinfo profileinfo(string FBid);
        #endregion


        #region MessageDetails
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/MessageDetails?FBid={Fbid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        MessageDetails messagedetails(string FBid);
        #endregion




        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.


    #region Search
    [DataContract]
    public class Search
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public List<FriendDetails> frienddetails { get; set; }
        [DataMember(Order = 4)]
        public List<InvitedFriends> invitedfriends { get; set; }
    }
    [DataContract]
    public class FriendDetails
    {
        [DataMember(Order = 1)]
        public string FriendsFBID { get; set; }
        [DataMember(Order = 2)]
        public string FriendsName { get; set; }
        [DataMember(Order = 3)]
        public string FriendsPropic { get; set; }
        [DataMember(Order = 4)]
        public string Status { get; set; }
    }
    [DataContract]
    public class InvitedFriends
    {
        [DataMember(Order = 1)]
        public string FriendsFBID { get; set; }
        [DataMember(Order = 2)]
        public string FriendsName { get; set; }
        [DataMember(Order = 3)]
        public string FriendsPropic { get; set; }
        [DataMember(Order = 4)]
        public string Status { get; set; }
    }
    [DataContract]
    public class SearchInput
    {

        [DataMember]
        public string fbid { get; set; }

    }
    #endregion

    #region  Angels login
    [DataContract]
    public class SneakyLogin
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }
    [DataContract]
    public class SneakyLogininput
    {
        [DataMember]
        public string Did { get; set; }
        [DataMember]
        public string Dtoken { get; set; }
        [DataMember]
        public string fbid { get; set; }
        [DataMember]
        public string FBtoken { get; set; }
        [DataMember]
        public string Lat { get; set; }
        [DataMember]
        public string Lon { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }

    }

    [DataContract]
    public class Friendslogininput
    {
        [DataMember]
        public string Did { get; set; }
        [DataMember]
        public string Dtoken { get; set; }
        [DataMember]
        public string fbid { get; set; }
        [DataMember]
        public string FBtoken { get; set; }
        [DataMember]
        public string Lat { get; set; }
        [DataMember]
        public string Lon { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }

    }
    #endregion

    #region deletesign
    [DataContract]
    public class Delete
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region Request
    [DataContract]
    public class Request
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region Approve
    [DataContract]
    public class Approve
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region SneakyInvite
    [DataContract]
    public class SneakyInvite
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string RequestedDate { get; set; }
    }

    #endregion

    #region SneakyReqDetails
    [DataContract]
    public class SneakyReqDetails
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string ImageStatus { get; set; }
        [DataMember(Order = 4)]
        public string Imageurl { get; set; }
        [DataMember(Order = 5)]
        public string RequestedDate { get; set; }
        [DataMember(Order = 6)]
        public string AcceptedDate { get; set; }
        [DataMember(Order = 7)]
        public string Latitude { get; set; }
        [DataMember(Order = 8)]
        public string Longitude { get; set; }
        [DataMember(Order = 9)]
        public string Sneakiedtimes { get; set; }
    }
    

    #endregion

    #region Sneakyinfo
    [DataContract]
    public class SneakyInfo
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string por_pic { get; set; }
        [DataMember(Order = 4)]
        public string FirstName { get; set; }
        [DataMember(Order = 5)]
        public string Sneakiedtimes { get; set; }
        [DataMember(Order = 6)]
        public string candid_status { get; set; }

    }

    #endregion

    #region SneakyMembers
    [DataContract]
    public class SneakyMembers
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public List<MemberDetails> memberdetails { get; set; }
    }
    [DataContract]
    public class MemberDetails
    {
        [DataMember(Order = 1)]
        public string FBID { get; set; }
        [DataMember(Order = 2)]
        public string imageurl { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string AcceptedDate { get; set; }
    }
    #endregion

    #region SneakyLocation
    [DataContract]
    public class SneakyLocation
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region Message
    [DataContract]
    public class SneakyMessage
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }

    #endregion

    #region  ImageUpload
    [DataContract]
    public class ImageUpload
    {
        [DataMember(Order = 1)]
        public string ReturnCode { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public string SneakyimgURL { get; set; }
    }
    [DataContract]
    public class ImageUploadInput
    {
        [DataMember]
        public string FBid { get; set; }
        [DataMember]
        public string imagedata { get; set; }
    }
    #endregion

    #region CandidDetails
    [DataContract]
    public class CandidDetails
    {
        [DataMember]
        public string Returncode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<Candidinfo> Candidinfo { get; set; }
    }
    [DataContract]
    public class Candidinfo
    {
        [DataMember]
        public string FBID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

#endregion

    #region CandidStatus
    [DataContract]
    public class CandidStatus
    {
        [DataMember]
        public string ReturnCode { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

#endregion

    #region sneakyimagelist
    [DataContract]
    public class SneakyImageList
    {
        [DataMember]
        public string Returncode { get; set;}
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<ImageDetails>imagedetails  {get; set;}

    }
    [DataContract]
    public class ImageDetails
    {
        [DataMember]
        public string imageurl { get; set; }
        [DataMember]
        public string FBID { get; set; }
        [DataMember]
        public string Firstname { get; set; }
        [DataMember]
        public string AcceptedDate { get; set; }
    }
#endregion

    #region Profileinfo
    [DataContract]
    public class Profileinfo
    {
        [DataMember]
        public string Returncode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string profilepic { get; set; }
        [DataMember]
        public string Sneaky_sent { get; set; }
        [DataMember]
        public string Sneaky_recieved { get; set; }
    }

#endregion

    #region MessageDetails
    [DataContract]
    public class MessageDetails
    {
        [DataMember]
        public string Returncode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<SentDetails> sentdetails { get; set; }
        [DataMember]
        public List<ReceivedDetails> recieveddetails { get; set; }
    }

    [DataContract]
    public class SentDetails
    {
        [DataMember]
        public string FBID { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string First_name { get; set; }
        [DataMember]
        public string Prof_pic { get; set; }
    }

    [DataContract]
    public class ReceivedDetails
    {
        [DataMember]
        public string FBID { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string First_name { get; set; }
        [DataMember]
        public string Prof_pic { get; set; }
    }
#endregion
}
