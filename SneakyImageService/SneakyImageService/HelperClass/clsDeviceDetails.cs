﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SneakyImageService.HelperClass
{
    public class clsDeviceDetails
    {
        public string Fromname
        {
            get;
            set;
        }
        public string Toname
        {
            get;
            set;
        }


        public string NotificationID
        {
            get;
            set;
        }

        public string DevToken
        {
            get;
            set;
        }
        public string Notification
        {
            get;
            set;
        }
        public string Profilepic
        {
            get;
            set;
        }
    }

}