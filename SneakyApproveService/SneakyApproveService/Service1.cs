﻿using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using SneakyApproveService.PushClass;
using SneakyApproveService.HelperClass;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace SneakyApproveService
{
    public partial class Service1 : ServiceBase
    {
        #region Declarations
        Thread _thPush = null;
        SqlConnection con1 = null;
        string connection = @"Data Source=DGSMOBILESMARTL\CLOUDMSSQL2008;Initial Catalog=Sneaky;Integrated Security=True;MultipleActiveResultSets=True";
        SqlDataReader objSqlReader = null;
        //SqlDataAdapter objSqlAdp = null;
        SqlCommand cmd1 = null;
        string str_qry = string.Empty;
        List<clsDeviceDetails> lstDevice = null;
        clsDeviceDetails objDevice = null;
        #endregion

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                writeLog(DateTime.Now.ToString(), " Service Started");
                con1 = new SqlConnection(connection);
                _thPush = new Thread(Execute);
                _thPush.IsBackground = true;
                _thPush.Start();
            }
            catch (Exception exc)
            {
                writeLog("OnStart_Error", exc.Message.ToString());
            }

        }

        public void Execute()
        {
            try
            {
                while (true)
                {
                    if (countbadges() > 0)
                    {
                        if (countdevicetoken() > 0)
                        {

                            pushNotification();
                        }
                    }
                    Thread.Sleep(2000);
                }
            }
            catch (ThreadAbortException thAE)
            {
                writeLog("Main Catch", "error");
            }
        }

        public int countbadges()
        {
            int count = 0;
            try
            {
                string qry = "select COUNT(*) as Notification from tbl_ReqTracker where  AppNotification=0 and Status='approved' and RecNotification=0";
                cmd1 = new SqlCommand();
                cmd1.Connection = con1;
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = qry;
                con1.Open();
                count = (int)cmd1.ExecuteScalar();
                con1.Close();
                writeLog("Count of Devices to notify", "Count: " + count + "");
            }
            catch (Exception ex)
            {
                //writeLog("On count Replied exception", ex.Message);
            }
            return count;

        }

        public int countdevicetoken()
        {
            try
            {
                writeLog("Onget_device_token", "Device token started");
                lstDevice = null; lstDevice = null;
                lstDevice = new List<clsDeviceDetails>();
                str_qry = "exec SP_ApproveNotification";
                cmd1 = new SqlCommand();
                cmd1.Connection = con1;
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = str_qry;
                con1.Open();
                objSqlReader = cmd1.ExecuteReader();
                while (objSqlReader.Read())
                {
                    objDevice = new clsDeviceDetails();
                    objDevice.Fromname = objSqlReader[0].ToString();
                    objDevice.Toname = objSqlReader[1].ToString();
                    objDevice.NotificationID = objSqlReader[2].ToString();
                    objDevice.DevToken = objSqlReader[3].ToString();
                    objDevice.Profile_pic = objSqlReader[4].ToString();
                    lstDevice.Add(objDevice);
                }
                writeLog("Dev_token:Count", lstDevice.Count.ToString());
                objSqlReader.Dispose();
                objSqlReader.Close();
                con1.Close();
            }
            catch (Exception exc)
            {
                writeLog("OnGetDeviceToken_Error", exc.Message.ToString());
            }
            return lstDevice.Count;
        }

        private void pushNotification()
        {
            string p12FilePassword = null;
            try
            {
                if (lstDevice.Count > 0)
                {
                    int badgeCount = 0;
                    string updateFlag = string.Empty;
                    foreach (clsDeviceDetails Device in lstDevice)
                    {

                        Notification alertNotification = null;
                        bool sandbox = true;

                        //Put your PKCS12 .p12 or .pfx filename here.
                        // Assumes it is in the same directory as your app
                        string p12File = @"C:\Sneaky\SneakyApproveService\SneakyApproveService\IPhoneCertificate\SneakyPush.p12";
                        //This is the password that you protected your p12File 
                        //  If you did not use a password, set it as null or an empty string
                        try
                        {
                            p12FilePassword = "123";
                            //p12FilePassword = ""; 
                        }
                        catch (Exception ex)
                        {
                            writeLog("Password", ex.Message);
                        }

                        writeLog("notifystart", "");
                        NotificationService service = new NotificationService(sandbox, p12File, p12FilePassword, 1);
                        //NotificationService service = new NotificationService("gateway.push.apple.com", 2195,p12File, 1);

                        service.ReconnectDelay = 8000; //8 seconds

                        #region"Raise Event"
                        //Event trigger
                        service.Error += new NotificationService.OnError(service_Error);
                        service.NotificationTooLong += new NotificationService.OnNotificationTooLong(service_NotificationTooLong);
                        service.BadDeviceToken += new NotificationService.OnBadDeviceToken(service_BadDeviceToken);
                        service.NotificationFailed += new NotificationService.OnNotificationFailed(service_NotificationFailed);
                        service.NotificationSuccess += new NotificationService.OnNotificationSuccess(service_NotificationSuccess);
                        service.Connecting += new NotificationService.OnConnecting(service_Connecting);
                        service.Connected += new NotificationService.OnConnected(service_Connected);
                        service.Disconnected += new NotificationService.OnDisconnected(service_Disconnected);
                        #endregion
                        writeLog("Divce token", Device.DevToken);
                        badgeCount = Convert.ToInt32(Device.Notification) + 1;
                        alertNotification = new Notification(Device.DevToken);
                        alertNotification.Tag = "FRNDACCPTD";
                        alertNotification.Payload.AddCustom("Profilepic", Device.Profile_pic);
                        alertNotification.Payload.AddCustom("Type", "FRNDACCPTD");
                        alertNotification.Payload.Alert.Body = "Congratulations! '" + Device.Toname + "'approved your friend request ";
                        alertNotification.Payload.Sound = "default";
                        alertNotification.Payload.Badge = badgeCount;

                        if (service.QueueNotification(alertNotification))
                        {
                            writeLog("OnPushNotification", "Notification Queued!");
                        }
                        else
                        {
                            writeLog("OnPushNotification_Error", "Notification Failed to be Queued!");
                        }
                        service.Close();

                        con1.Open();
                        updateFlag = "update tbl_ReqTracker set AppNotification=1 where ID='" + Device.NotificationID + "'";
                        writeLog(updateFlag, "");
                        cmd1.CommandType = CommandType.Text;
                        cmd1.CommandText = updateFlag;
                        int AffrectedRows = cmd1.ExecuteNonQuery();
                        con1.Close();
                        writeLog("AffectedRows", AffrectedRows.ToString());
                        writeLog("OnPushNotification", "sent");
                    }
                }
            }
            catch (Exception ex)
            {
                writeLog("OnPushNotification", "exception:" + ex.Message);
            }
        }

        protected override void OnStop()
        {
            try
            {
                writeLog("OnStop", " Service Stopped");
                _thPush.Abort();
                _thPush = null;
            }
            catch (ThreadAbortException thAB)
            {
                writeLog("OnStop_Error", thAB.Message.ToString());
            }
        }

        #region "Events"ss
        public void service_BadDeviceToken(object sender, BadDeviceTokenException ex)
        {
            writeLog("service_BadDeviceToken", ex.Message.ToString());
        }

        public void service_Disconnected(object sender)
        {
            writeLog("service_Disconnected", "Disconnected");
        }

        public void service_Connected(object sender)
        {
            writeLog("service_Connected", "Connected");
        }

        public void service_Connecting(object sender)
        {
        }

        public void service_NotificationTooLong(object sender, NotificationLengthException ex)
        {

            writeLog("service_NotificationTooLong", ex.Notification.ToString());
        }

        public void service_NotificationSuccess(object sender, Notification notification)
        {
            writeLog("service_NotificationSuccess", notification.ToString());
        }

        public void service_NotificationFailed(object sender, Notification notification)
        {
            writeLog("service_NotificationFailed", notification.ToString());
        }

        public void service_Error(object sender, Exception ex)
        {
            writeLog("service_Error", ex.Message.ToString());
        }
        #endregion

        #region Logfile
        private void writeLog(string MsgID, string Msg)
        {
            string openString = DateTime.Now.ToString();
            string path = @"C:\Sneaky\SneakyApproveService\SneakyApproveService\Log\log" + DateTime.Now.ToString("dd-MM-yy") + ".txt";

            if (File.Exists(path))
            {
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(openString + " -- (" + MsgID + ") -- " + Msg);
                    writer.Close();
                }
            }
            else
            {
                File.WriteAllText(path, openString + " Service Started");
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(openString + " -- (" + MsgID + ") -- " + Msg);
                    writer.Close();
                }
            }
        }
        #endregion
    }
}
